package com.example.animaltp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView view = (RecyclerView) findViewById(R.id.list);
        AnimalAdapter adapter = new AnimalAdapter(AnimalList.getNameArray(), view.getContext());
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(this));
    }
}
