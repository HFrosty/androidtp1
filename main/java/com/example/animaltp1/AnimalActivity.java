package com.example.animaltp1;

import android.content.Intent;
import android.net.Uri;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

public class AnimalActivity extends AppCompatActivity {

    private Animal currentAnimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Intent intent = getIntent();
        if(intent != null)
        {
            Animal animal = AnimalList.getAnimal(intent.getStringExtra("name"));
            currentAnimal = animal;

            // Display animal information
            ((TextView) findViewById(R.id.name)).setText(intent.getStringExtra("name"));
            ((ImageView) findViewById(R.id.image)).setImageResource(getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName()));
            ((TextView) findViewById(R.id.lifespan)).setText(animal.getStrHightestLifespan());
            ((TextView) findViewById(R.id.pregnancy)).setText(animal.getStrGestationPeriod());
            ((TextView) findViewById(R.id.birthWeight)).setText(animal.getStrBirthWeight());
            ((TextView) findViewById(R.id.adultWeight)).setText(animal.getStrAdultWeight());
            ((EditText) findViewById(R.id.conservationStatus)).setText(animal.getConservationStatus());
        }
    }

    public void saveData(View view)
    {
        // Retrieve data
        String newConservationStatus = ((TextView) findViewById(R.id.conservationStatus)).getText().toString();

        currentAnimal.setConservationStatus(newConservationStatus);
    }
}
