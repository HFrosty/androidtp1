package com.example.animaltp1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalAdapter extends RecyclerView.Adapter<AnimalAdapter.MyViewHolder> {
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ImageView imageView;
        public MyViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.animalName);
            imageView = (ImageView) itemView.findViewById(R.id.animalImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String animalName = textView.getText().toString();
                    Intent intent = new Intent(view.getContext(), AnimalActivity.class);
                    intent.putExtra("name", animalName);
                    view.getContext().startActivity(intent);                }
            });
        }
    }

    private String[] data;
    private Context context;

    public AnimalAdapter(String[] data, Context context)
    {
        this.data = data;
        this.context = context;
    }

    @Override
    public AnimalAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.animal_list_element, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AnimalAdapter.MyViewHolder viewHolder, int position) {
        Animal animal = AnimalList.getAnimal(data[position]);

        TextView textView = viewHolder.textView;
        ImageView imageView = viewHolder.imageView;

        textView.setText(data[position]);
        imageView.setImageResource(context.getResources().getIdentifier(animal.getImgFile(), "drawable", context.getPackageName()));
    }

    @Override
    public int getItemCount() {
        return data.length;
    }
}
